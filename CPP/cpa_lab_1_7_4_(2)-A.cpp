﻿
#include "pch.h"
#include <iostream>
#include <cmath>

using namespace std;

int main(void)
{

	printf("Please enter values:\n");
	float a;
	float b;

	cin >> a;
	cin >> b;

	if (fabs(1 / a) - 1 / b < 0.000001) {
		cout << "Results are equal (by 0.000001 epsilon)";
	}
	else {
		cout << "Results are not equal (by 0.000001 epsilon)";
	}
}

