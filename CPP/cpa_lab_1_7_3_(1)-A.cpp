﻿
#include "pch.h"
#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	printf("Put these values into your code:\n");
	int a;
	double b;
	double c;
	double d;
	double f;

	cin >> b;
	cout <<  b << endl;

	cin >> c;
	cout << fixed << setprecision(2) << c << endl;

	cin >> d;
	cout << fixed << setprecision(6) << d << endl;

	cin >> f;
	cout << fixed << setprecision(2) << f << endl;

	cin >> a;
	cout << a;


}
