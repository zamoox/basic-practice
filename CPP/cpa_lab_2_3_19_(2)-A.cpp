﻿
#include "pch.h"
#include <iostream>

using namespace std;

int main(void)
{
	double pi4 = 0.;
	double pi = 0.0;
	long  n;
	long pr = 1;

	cout << "Number of iterations? ";
	cin >> n;

	for (long i = 1; i < n; i = i + 2) {

		pi = 1.0 / i;

		if (pr % 2 != 0)
		{
			pi4 += pi;
		}
		else {
			pi4 -= pi;
		}
		pr++;
	}

	cout.precision(20);
	cout << "Pi = " << (pi4 * 4.) << endl;
	return 0;
}

