﻿

#include "pch.h"
#include <iostream>

using namespace std;

int main(void)
{
	int year;
	int month;
	int day;
	int day_week;

	cout << "Enter value year|month|day\n";
	cin >> year;
	cin >> month;
	cin >> day;

	month -= 2;

	if (month < 0) {
		month += 12;
		year -= 1;
	}
	month = (month * 83)/32;
	month += day;
	month += year;
	month += year / 4;
	month -= year / 100;
	month += year / 400;
	day_week = month % 7;
	cout << day_week;
}

