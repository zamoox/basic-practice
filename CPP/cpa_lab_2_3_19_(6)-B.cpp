﻿

#include "pch.h"
#include <iostream>

using namespace std;

int main(void)
{
	int n;
	cout << "Enter a high of pyramid:\n ";
	cin >> n;
	if (n > 2 && n < 9)
	{
		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j <= n * 2; j++)
			{
				if (j == ((n + 1) - (i - 1)) || j == ((n + 1) + (i - 1)))
					cout << '*';
				else
					if (i == n && j > 1)
						cout << '*';
					else
						cout << ' ';
			}

			cout << endl;
		}
	}
	else { cout << "n has a wrong range\n"; }

	return 0;
}

